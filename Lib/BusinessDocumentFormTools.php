<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\CalcularArea\Lib;

use FacturaScripts\Core\Lib\BusinessDocumentFormTools as ParentBusiness;
use FacturaScripts\Core\Model\Variante;
use FacturaScripts\Core\Model\Producto;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of BusinessDocumentFormTools
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class BusinessDocumentFormTools extends ParentBusiness
{
    protected function recalculateFormLine(array $fLine, \FacturaScripts\Core\Model\Base\BusinessDocument $doc) {
        if (isset($fLine['idproducto'])) {
            $product = new Producto();
            $product->loadFromCode($fLine['idproducto']);

            $variant = new Variante();
            $where = [
                new DataBaseWhere('referencia', $fLine['referencia'])
            ];
            $variant->loadFromCode('', $where);

            if (!is_null($product->calculatearea)) {
                $long = isset($fLine['long'])?$fLine['long']:0;
                $width = isset($fLine['width'])?$fLine['width']:0;
                $height = isset($fLine['height'])?$fLine['height']:0;
                $result = 0;

                if ($long > 0 && $width > 0) {
                    $result = $long * $width;
                }

                if ($result > 0 && $height > 0) {
                    $result = $result * $height;
                }

                if ($result > 0) {
                    switch ($product->calculatearea) {
                        case 1:
                            $fLine['cantidad'] = $result;
                            break;
                        case 2:
                            $fLine['pvpunitario'] = $result * $variant->precio;
                            break;
                    }
                }
            } else {
                unset($fLine['long']);
                unset($fLine['width']);
                unset($fLine['height']);
            }
        } 
        
        return parent::recalculateFormLine($fLine, $doc);
    }
}