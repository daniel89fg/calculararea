<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\CalcularArea\Lib\PDF;

use FacturaScripts\Core\Lib\PDF\PDFDocument as ParentPDF;

/**
 * Description of PDFDocument
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
abstract class PDFDocument extends ParentPDF
{
    public function __construct() {
        parent::__construct();
        $this->setCustomLineHeader('long', ['type' => 'number', 'title' => $this->i18n->trans('long')], 2);
        $this->setCustomLineHeader('width', ['type' => 'number', 'title' => $this->i18n->trans('width')], 3);
        $this->setCustomLineHeader('height', ['type' => 'number', 'title' => $this->i18n->trans('height')], 4);
    }
    
    /**
     * 
     * @param string $colum
     * @param array $dataArray
     * @param int $position
     *
     */
    function setCustomLineHeader($colum, $dataArray, $position)
    {
        if ($position >= count($this->lineHeaders)) {
            $this->lineHeaders[$colum] = $dataArray;
            return false;
        }
        
        if ($position < 0) {
            $position = 0;
        }
        
        $newArray = array();
        
        $cont = 0;
        foreach ($this->lineHeaders as $key => $value) {
            if($cont === $position) {
                $newArray[$colum] = $dataArray;
            }
            $newArray[$key] = $value;
            $cont++;
        }
        $this->lineHeaders = $newArray;
    }
}