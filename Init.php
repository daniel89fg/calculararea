<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\CalcularArea;

use FacturaScripts\Core\Base\InitClass;

/**
 * Description of Init
 *
 * @author Daniel Fernández Giménez <daniel.fernandez@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        
    }

    public function update()
    {
        
    }
}
